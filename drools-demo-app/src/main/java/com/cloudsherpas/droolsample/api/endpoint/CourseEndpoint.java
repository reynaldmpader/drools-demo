package com.cloudsherpas.droolsample.api.endpoint;

import com.cloudsherpas.droolsample.api.exception.InvalidParameterException;
import com.cloudsherpas.droolsample.api.resource.SuggestionResource;
import com.cloudsherpas.droolsample.service.CourseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author RMPader
 */
@Api
@Component
@Path("/course")
public class CourseEndpoint {

    @Autowired
    private CourseService courseService;

    @GET
    @Path("/suggest")
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "Get Course Suggestions",
                  notes = "Returns a list of applicable courses based on your preferences for each field. " +
                          "Currently supported Fields are 'math', 'software', 'electronics', 'arts', 'social_studies'.",
                  response = SuggestionResource.class)
    public SuggestionResource suggestCourse(@Context UriInfo uriInfo) {
        try {
            MultivaluedMap<String, String> ratings = uriInfo.getQueryParameters();
            Map<String, Integer> r = ratings.entrySet()
                                            .stream()
                                            .collect(Collectors.toMap(Map.Entry::getKey,
                                                                      e -> Integer.valueOf(e.getValue()
                                                                                            .get(0))));

            return courseService.suggestCourses(r);
        } catch (IllegalArgumentException e) {
            throw new InvalidParameterException();
        }
    }
}
