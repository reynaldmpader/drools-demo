package com.cloudsherpas.droolsample.api.endpoint;

import com.cloudsherpas.droolsample.api.resource.ArtifactActivationResource;
import com.cloudsherpas.droolsample.api.resource.RuleArtifactResource;
import com.cloudsherpas.droolsample.data.domain.RuleArtifact;
import com.cloudsherpas.droolsample.service.RuleAdminService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.List;

/**
 * @author RMPader
 */
@Api
@Component
@Path("/admin")
public class AdminEndpoint {

    @Autowired
    private RuleAdminService ruleAdminService;

    @GET
    @Path("/rules")
    @Produces({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "Get Rule Artifacts",
                  notes = "Fetch a list of available rule artifacts.",
                  response = RuleArtifact.class,
                  responseContainer = "List")
    public List<RuleArtifact> getArtifacts() throws IOException {
        return ruleAdminService.getArtifacts();
    }

    @POST
    @Path("/rules/activate")
    @Consumes({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "Activate Rule Artifact",
                  notes = "Set the application to use the specified rule artifact.")
    public void activateRuleArtifact(ArtifactActivationResource artifactActivationResource) throws IOException {
        ruleAdminService.activateRuleArtifact(artifactActivationResource);
    }

    @POST
    @Path("/rules/add")
    @Consumes({MediaType.APPLICATION_JSON})
    @ApiOperation(value = "Add Rule Artifact",
                  notes = "Add a rule artifact for reference. Use the Activation endpoint to use the artifact.")
    public void addRuleArtifact(RuleArtifactResource ruleArtifactResource) throws IOException {
        ruleAdminService.addRuleArtifact(ruleArtifactResource);
    }

}
