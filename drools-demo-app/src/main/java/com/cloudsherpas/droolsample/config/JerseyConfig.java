package com.cloudsherpas.droolsample.config;

import com.cloudsherpas.droolsample.api.endpoint.AdminEndpoint;
import com.cloudsherpas.droolsample.api.endpoint.CourseEndpoint;
import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.listing.ApiListingResource;
import io.swagger.jaxrs.listing.SwaggerSerializers;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import javax.ws.rs.ApplicationPath;

/**
 * @author RMPader
 */
@Configuration
@ApplicationPath("/api")
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        configureSwagger();
        register(CourseEndpoint.class);
        register(AdminEndpoint.class);
    }

    private void configureSwagger() {
        register(ApiListingResource.class);
        register(SwaggerSerializers.class);
        BeanConfig beanConfig = new BeanConfig();
        beanConfig.setVersion("1.0.0");
        beanConfig.setSchemes(new String[]{"http"});
        beanConfig.setHost("localhost:8081");
        beanConfig.setBasePath("/api");
        beanConfig.setResourcePackage("com.cloudsherpas.droolsample.api.endpoint");
        beanConfig.setPrettyPrint(true);
        beanConfig.setScan(true);
    }
}
